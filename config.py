# Statement for enabling the development environment
import os
DEBUG = True

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Define database configurations
# 'mysql+pymysql://root:P2iH@4282db@localhost/test_flask'
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:P2iH@4282db@localhost/test_flask'
SQLALCHEMY_TRACK_MODIFICATIONS = False
