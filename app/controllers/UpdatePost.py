from flask import request, jsonify
from app.shared.models import db
from app.models.post import Post
from app.models.postSch import PostSchema
from app.controllers.BlogPosts import blog_posts
import uuid
import datetime


def update_post():
    try:
        post_id = request.args.get('post_id')
        title = request.json['title']
        content = request.json['content']

        post = Post.query.get(post_id)

        if(post):
            post.title = title
            post.content = content
            db.session.commit()
            return jsonify({'success': True, 'message': 'updated successfully'})
        return jsonify({'success': False, 'message': 'fail to update'})
    except:
        return jsonify({'message': 'something wrong'})
