from flask import request, jsonify
from app.shared.models import db
from app.models.post import Post
from app.models.postSch import PostSchema
from app.controllers.BlogPosts import blog_posts
import uuid
import datetime


def add_post():
    try:
        post_id = uuid.uuid1().hex
        title = request.json['title']
        content = request.json['content']
        public = 1 if request.json['public'] == True else 0
        user_id = request.args.get('user_id')
        posted_at = datetime.datetime.utcnow()
        new_post = Post(post_id, title, content, public, posted_at, user_id)

        db.session.add(new_post)
        db.session.commit()

        return blog_posts()
    except:
        return jsonify({'message': 'something wrong'})
