from flask import request, jsonify
from app.shared.models import db
from app.models.user import User
from app.models.userSch import UserSchema
from passlib.hash import sha256_crypt
import uuid
import datetime


def sign_up():
    try:
        id = uuid.uuid1().hex
        name = request.json['name']
        email = request.json['email']
        password = request.json['password']
        c_password = request.json['c_password']

        is_exist_user = User.query.filter_by(email=email).all()

        if(is_exist_user):
            return jsonify({'message': 'email is already exist'})
        if(password != c_password):
            return jsonify({'message': 'password does not match'})

        password = sha256_crypt.encrypt(password)
        new_user = User(id, name, email, password)
        db.session.add(new_user)
        db.session.commit()

        return jsonify({'message': 'successfully registered'})
    except:
        return jsonify({'message': 'something wrong'})
