from flask import request, jsonify
from app.shared.models import db
from app.models.post import Post
from app.models.postSch import PostSchema
from app.controllers.BlogPosts import blog_posts
import uuid
import datetime


def delete_post():
    try:
        post_id = request.args.get('post_id')
        post = Post.query.get(post_id)

        if(post):
            db.session.delete(post)
            db.session.commit()

            return jsonify({'success': True, 'message': 'Successfully deleted'})

        return jsonify({'success': False, 'message': 'Invalid request'})
    except:
        return jsonify({'message': 'something wrong'})
