from flask import request, jsonify
from passlib.hash import sha256_crypt
from app.models.user import User
from app.models.userSch import UserSchema


def sign_in():
    try:
        req_email = request.json['email']
        req_password = request.json['password']

        user_schema = UserSchema()
        #user = user_schema.dump((User.query.get(req_email)))
        user = User.query.filter(User.email == req_email).first()
        #user = user_schema.dump(user)

        if(user):
            if(sha256_crypt.verify(req_password, user.password)):
                user = user_schema.dump(user)
                return jsonify({'data': user.data})

            return jsonify({'message': 'Invalid password'})

        return jsonify({'message': 'Authentication error'})
    except:
        return jsonify({'message': 'something wrong'})
