from flask import request, jsonify
from app.models.post import Post
from app.models.postSch import PostSchema


def blog_posts():
    try:
        posts_schema = PostSchema(many=True)
        posts = posts_schema.dump(Post.query.filter(Post.public == 1).all())
        return jsonify(posts.data)
    except:
        return jsonify({'message': 'something wrong'})
