from flask import request, jsonify
from app.shared.models import db
from app.models.blogComment import Comment
#from app.models.postSch import PostSchema
#from app.controllers.BlogPosts import blog_posts
import uuid
import datetime


def add_comment():
    try:
        id = uuid.uuid1().hex
        comment = request.json['comment']
        visitor_name = request.json['visitor_name']
        visitor_email = request.json['visitor_email']
        post_id = request.args.get('post_id')
        new_comment = Comment(id, comment, post_id,
                              visitor_name, visitor_email)

        db.session.add(new_comment)
        db.session.commit()

        return jsonify({'message': 'comment added'})
    except:
        return jsonify({'message': 'something wrong'})
