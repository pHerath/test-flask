from app.shared.models import db
from app.models.post import Post


class Comment(db.Model):
    __tablename__ = 'blog_comment'

    id = db.Column(db.String(64), primary_key=True)
    comment = db.Column(db.String())
    post_id = db.Column(db.String(64), db.ForeignKey("posts.post_id"))
    visitor_name = db.Column(db.String(255))
    visitor_email = db.Column(db.String(100))
    post = db.relationship("Post", backref=db.backref('posts', lazy='dynamic'))

    def __init__(self, id, comment, post_id, visitor_name, visitor_email):
        self.id = id
        self.comment = comment
        self.post_id = post_id
        self.visitor_name = visitor_name
        self.visitor_email = visitor_email
