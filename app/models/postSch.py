from app.shared.schemas import ma
from marshmallow import Schema, fields, ValidationError, pre_load
from app.models.userSch import UserSchema

# class PostSchema(ma.Schema):
#     class Meta:
#         fields = ('post_id', 'title', 'content', 'posted_at',
#                   'user_id')


class PostSchema(ma.Schema):
    post_id = fields.Str(dump_only=True)
    user = fields.Nested(UserSchema, only=('id', 'name', 'email'))
    title = fields.Str(required=True)
    content = fields.Str(required=True)
    posted_at = fields.DateTime(dump_only=True)
