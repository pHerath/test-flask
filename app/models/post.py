from app.shared.models import db
from app.models.user import User


class Post(db.Model):
    __tablename__ = 'posts'

    post_id = db.Column(db.String(64), primary_key=True)
    title = db.Column(db.String(255))
    content = db.Column(db.String(255))
    posted_at = db.Column(db.DateTime, nullable=False)
    public = db.Column(db.Integer, nullable=False)
    user_id = db.Column(db.String(64), db.ForeignKey("users.id"))
    user = db.relationship("User", backref=db.backref('users', lazy='dynamic'))

    def __init__(self, post_id, title, content, public, posted_at, user_id):
        self.post_id = post_id
        self.title = title
        self.content = content
        self.posted_at = posted_at
        self.public = public
        self.user_id = user_id
