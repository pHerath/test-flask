from flask import Blueprint, render_template, session, abort
from app.controllers.BlogPosts import blog_posts
from app.controllers.Login import sign_in
from app.controllers.Register import sign_up
from app.controllers.AddPost import add_post
from app.controllers.UpdatePost import update_post
from app.controllers.DeletePost import delete_post
from app.controllers.AddComment import add_comment


home = Blueprint('home', __name__)
login = Blueprint('login', __name__)
register = Blueprint('register', __name__)
addPost = Blueprint('addPost', __name__)
editPost = Blueprint('editPost', __name__)
deletePost = Blueprint('deletePost', __name__)
addComment = Blueprint('addComment', __name__)


@home.route('/blog-posts', methods=['GET'])
def default():
    return blog_posts()


@login.route('/blog-posts/sign-in', methods=['POST'])
def singIn():
    return sign_in()


@register.route('/blog-posts/sign-up', methods=['POST'])
def signUp():
    return sign_up()


@addPost.route('/blog-posts', methods=['POST'])
def add():
    return add_post()


@editPost.route('/blog-posts', methods=['PUT'])
def edit():
    return update_post()


@deletePost.route('/blog-posts', methods=['DELETE'])
def delete():
    return delete_post()


@addComment.route('/blog-posts/comment', methods=['POST'])
def add_com():
    return add_comment()
