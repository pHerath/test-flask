from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from app.api import route

# Define the WSGI application object
app = Flask(__name__)
# mysql = MySQL(app)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:P2iH@4282db@localhost/test_flask'
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Configurations
app.config.from_object('config')

db = SQLAlchemy(app)
ma = Marshmallow(app)

app.register_blueprint(route.home)
app.register_blueprint(route.login)
app.register_blueprint(route.register)
app.register_blueprint(route.addPost)
app.register_blueprint(route.editPost)
app.register_blueprint(route.deletePost)
app.register_blueprint(route.addComment)
# @app.route("/blog-posts", methods=['GET'])
# def default():
# users_schema = UserSchema(many=True)
# users = users_schema.dump(User.query.all())
# posts_schema = PostSchema(many=True)
# posts = posts_schema.dump(Post.query.all())
# return jsonify(posts.data)


# @app.route('/<post_id>', methods=['GET'])
# def get_post(post_id):
#     post = Post.query.get(post_id)

#     if(post):
#         post_schema = PostSchema()
#         post = post_schema.dump(post)
#         return jsonify({'success': True, 'data': post.data})

#     return jsonify({'success': False, 'message': 'No post'})


# @app.route("/blog-posts/login", methods=['POST'])
# def login():
#     req_email = request.json['email']
#     req_password = request.json['password']

#     user_schema = UserSchema()
#     user = user_schema.dump((User.query.get(req_email)))
#     if(user.password):
#         return jsonify({"data": user})


# if(user):
#         if(sha256_crypt.verify(user.data.password, req_password)):
#             return jsonify({'message': 'successfully login'})

#         return jsonify({'message': 'invalid password'})

#     return jsonify({'message': 'Unauthorized'})


# @app.route("/user/register", methods=['POST'])
# def register():
#     id = uuid.uuid1().hex
#     name = request.json['name']
#     email = request.json['email']
#     password = request.json['password']
#     c_password = request.json['c_password']

#     is_exist_user = User.query.filter_by(email=email).all()

#     if(is_exist_user):
#         return jsonify({'message': 'email is already exist'})
#     if(password != c_password):
#         return jsonify({'message': 'password does not match'})

#     password = sha256_crypt.encrypt(password)
#     new_user = User(id, name, email, password)
#     db.session.add(new_user)
#     db.session.commit()

#     return jsonify({'message': 'successfully registered'})


# @app.route("/user/post/add/<user_id>", methods=['POST'])
# def add_post(user_id):
#     post_id = uuid.uuid1().hex
#     title = request.json['title']
#     content = request.json['content']
#     user_id = user_id
#     posted_at = datetime.datetime.utcnow()
#     new_post = Post(post_id, title, content, posted_at, user_id)

#     db.session.add(new_post)
#     db.session.commit()

#     return default()


# @app.route('/user/post/<post_id>', methods=['GET'])
# def post(post_id):
#     post = Post.query.get(post_id)

#     if(post):
#         post_schema = PostSchema()
#         post = post_schema.dump(post)
#         return jsonify({'success': True, 'data': post.data})

#     return jsonify({'success': False, 'message': 'No post'})


# @app.route('/user/post/edit', methods=['PUT'])
# def update_post():
#     post_id = request.json['post_id']
#     title = request.json['title']
#     content = request.json['content']

#     post = Post.query.get(post_id)

#     if(post):
#         post.title = title
#         post.content = content
#         db.session.commit()
#         return jsonify({'success': True, 'message': 'updated successfully'})
#     return jsonify({'success': False, 'message': 'fail to update'})


# @app.route('/user/post/delete/<post_id>', methods=["DELETE"])
# def delete_post(post_id):
#     post = Post.query.get(post_id)

#     if(post):
#         db.session.delete(post)
#         db.session.commit()

#         return jsonify({'success': True, 'message': 'Successfully deleted'})

#     return jsonify({'success': False, 'message': 'cannot delete'})

# @app.route('/post/<post_id>', methods=['GET', 'POST'])
# def get_post(post_id):
#     return "requested post id is %s" % post_id


# @app.route('/<path:path>')
# def set_url(path):
#     return 'requested path is %s' % path


if __name__ == "__main__":
    app.run(debug=True)
